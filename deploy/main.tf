terraform {
  backend "s3" {
    bucket         = "recipe-sat-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-sat-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}


locals {
  prefix = "${var.prefix}-${terraform.workspace}"
}